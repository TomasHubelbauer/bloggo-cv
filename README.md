# Tomas Hubelbauer

![Tomas Hubelbauer](photo.jpg)

> Software Development Consultant from Prague

## Current Experience (2016 on)

I work as a FE architect at Massive Interactive for 2 years now.

My role consists of responsibilities ranging from ensuring quality of development in terms of code and delivery
across various platforms (differs from project to project, most commonly web, iOS, tvOS, Android and TV platforms),
estimating work to be done and reviewing business stories in cooperation with project managers and business analysts,
1:1ing with developers and leads to ensure everyone is on the same page and blockers are being resolved and
mentoring developers on our proprietary product as well as software development in general.

The dev tech I get to work with on the daily or close to daily includes:

TypeScript, React, Redux, NodeJS, Swift, Java, Swagger, iOS, tvOS, Cordova, Haxe, TVs, gaming consoles, …

Aside from dev tech I also know my way around Bamboo, Octopus, Jira, Confluence, TestRail, HockeyApp, Play Store, App Store, …

I also ocassionally speak and mentor, these days mostly in private session with people looking to get into IT.
I used to work as a lecturer in Gopas, a company providing technical training to software developers and focused on .NET and ASP .NET.

I use Windows, Ubuntu and macOS on daily basis.

In my spare time I try to teach myself Rust and explore subjects of my expertize more in-depth. I also dabble with dev ops.

## Previous Experience (2011 - 2016)

The build of tech that I was introduced to or was able to improve in during these past endeavours includes:

.NET (C#, EF), SQL, PG, JavaScript and related (WebPack, Babel, Redux, TypeScript, Parcel), Swift, NodeJS, WPF, WinForms, Win32, WebRTC, SIP…

In 2016 I have also worked at Sprinx where I helped revamp a CMS solution based on modern .NET and fullstack technologies and approaches.

In 2016 I have worked at STRV, where I was working with NodeJS, .NET, C# and many interesting commercial integrations to a financial product.

From 2014 to 2016 I have worked at Mluvii, where I was developing interactive WebRTC video-based communication and colaboration apps.

In 2014 I worked at Skylab where I had a chance to flex my ASP .NET muscle as a CMS developer for a properietary product.

From 2011 to 2014 I was able to help develop a software for industrial paint automation and management solution, which was very interesting.

I pretty much blended from the odd part time job to almost full time while getting my education, so it's hard to pin point the precise start
to what would be my career, but the above are position which my genuine and keen personal interest in software development allowed me to secure.
